# CHANGELOG


## v0.28.1 (2025-02-26)

### Bug Fixes

- Rewrite locations in portalconfig with update-imex
  ([`ac07309`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/ac07309d9605accbe61767625d12c0dd018e5fdd))

### Refactoring

- Organize imports
  ([`fd70883`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/fd708831788a95d57f7537b4cbefab8e14c70dea))


## v0.28.0 (2025-02-20)

### Features

- Use new tgclients lib and APIs for publish and user management
  ([`4613c9a`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/4613c9abc7de46d424f8d228b546c1f782abcee5))


## v0.27.1 (2025-02-20)

### Bug Fixes

- Writing modified portalconfig requires registering namespaces in element tree
  ([`c767013`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/c76701333a45d3af05e1f0386eff145d289ee0c1))


## v0.27.0 (2025-02-17)

### Features

- **publish**: Allow publishing of technical objects / worldreadables
  ([`18d586f`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/18d586fc0a2dcdb1295acc2e78abcd0d18d1b7bd))


## v0.26.0 (2025-02-17)

### Chores

- Typo
  ([`ab1d382`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/ab1d382858d765030f5ea8a958a3e2c01687f6d8))

### Continuous Integration

- **gitlab**: Try to fix publish pipeline by using old version of semantic-release
  ([`459ce67`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/459ce6712446f5a8b652b7bed77629615db79ae5))

### Features

- Added commands to add, remove and list roles in projects and to search for eppns
  ([`4aa9be0`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/4aa9be083ca0df02c9bed480a4f78b7b178000ad))


## v0.25.0 (2025-02-14)

### Chores

- Merge main
  ([`f218bfe`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/f218bfec30d566d8e07739c4f621ef707aa1b511))

- Add missing license headers
  ([`651fcb3`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/651fcb391a2f8c325fe30f7650085384cfbddb7f))

### Code Style

- Add ruff autoformat rules, precommit-hooks and editorconfig to enforce code and doc style
  ([`c610d00`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/c610d00ef586468a0aa62d9c0972db58d87885b7))

### Features

- Project autompletion for more commands
  ([`e6f1a29`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/e6f1a29e676c600068a26b1e132a6215b61bbcd6))

- Let autocompletion for project IDs choose server
  ([`8fa3557`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/8fa355736b88c718d1fd3445450d8f7a799608e9))

### Testing

- Add missing assert
  ([`2b61f32`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/2b61f328e6e1a331a9b29844e34ca14b975d8f50))

- Add basic integration tests
  ([`43bf052`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/43bf0522b46e189997635e38f4f1f4b200cca2f6))


## v0.24.0 (2025-02-13)

### Features

- Parse ondisk metadata with and without containertype. reorder params for put, to be consistent
  with put-aggregation
  ([`3304521`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/330452114562b1325f3a1c508261d7a96e73e0dc))

BREAKING CHANGE: project id comes before filename(s) now for put command


## v0.23.0 (2025-02-13)

### Continuous Integration

- **gitlab**: Do not run pipeline on main when semantic release created new version
  ([`cc8be45`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/cc8be4539e50ed83b53b414c2a32cdadf31da93a))

### Features

- Shell completion for listing projects content. WIP: only for prod system right now
  ([`9d1fc46`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/9d1fc46385dd539e4179986f2e4c16d090b5e760))


## v0.2.3 (2024-02-23)

### Bug Fixes

- **gitlab-ci**: Missed workflow config
  ([`6c7aba2`](https://gitlab.gwdg.de/dariah-de/textgridrep/tgadmin/-/commit/6c7aba22ef8f6e215f9caa0652941984f58e989d))
