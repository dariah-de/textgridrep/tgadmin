# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os

from click.testing import CliRunner

from tgadmin.tgadmin import cli

# we assume that the env variables TEXTGRID_SID and PROJECT_ID are set
def test_portalconfig_upload_list_clean(tmp_path):

    project_id = os.getenv('PROJECT_ID')
    os.chdir(tmp_path)

    runner = CliRunner()

    # get portalconfig example files
    result = runner.invoke(cli,['portalconfig', '--xslt'])
    assert result.exit_code == 0
    pc = tmp_path / "portalconfig.xml"
    assert pc.exists()

    # put files online
    result = runner.invoke(cli,['put-aggregation', project_id, 'portalconfig.xml', 'Other_Files.collection', 'README.md', '--ignore-warnings'])
    assert result.exit_code == 0
    imex = tmp_path / "Other_Files.collection.imex"
    assert imex.exists()

    # update files with imex
    result = runner.invoke(cli,['update-imex', 'Other_Files.collection.imex'])
    assert result.exit_code == 0

    # clean project
    result = runner.invoke(cli,['clean', project_id, '--yes'])
    assert result.exit_code == 0

